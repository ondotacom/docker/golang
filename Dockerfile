FROM golang:1.23.2-alpine3.19
# Update certificatesa and install curl
RUN apk update && \
    apk upgrade && \
    apk add --no-cache ca-certificates curl && \
    update-ca-certificates 2>/dev/null || true
# Install golangci-lint and goose
RUN wget -O- -nv https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s v1.60.3 && \
    curl -fsSL https://raw.githubusercontent.com/pressly/goose/master/install.sh | sh
# This specific workdir is important for go.mod functionality
WORKDIR /usr/src/app